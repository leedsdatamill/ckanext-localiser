from setuptools import setup, find_packages
import sys, os

version = '0.1'

setup(
	name='ckanext-localiser',
	version=version,
	description="Improving CSV files with lat/lng data",
	long_description="""\
	""",
	classifiers=[], # Get strings from http://pypi.python.org/pypi?%3Aaction=list_classifiers
	keywords='',
	author='Ross Jones',
	author_email='ross@servercode.co.uk',
	url='https://bitbucket.org/leedsdatamill/ckanext-localiser',
	license='',
	packages=find_packages(exclude=['ez_setup', 'examples', 'tests']),
	namespace_packages=['ckanext', 'ckanext.localiser'],
	include_package_data=True,
	zip_safe=False,
	install_requires=[
	],
	entry_points=\
	"""
    [ckan.plugins]
	localiser=ckanext.localiser.plugin:LocaliserPlugin
	""",
)
