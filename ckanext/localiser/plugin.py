from logging import getLogger
from pylons import config

import ckan.plugins as p
import ckan.plugins.toolkit as toolkit

log = getLogger(__name__)



class LocaliserPlugin(p.SingletonPlugin):
    '''
    Plugin for adding lat/lng data to CSV files
    '''
    p.implements(p.IConfigurer)
    p.implements(p.IRoutes)
    p.implements(p.ITemplateHelpers)

    def before_map(self, map):
        controller = 'ckanext.localiser.controllers:LocalController'
        map.connect('localise_resource', '/dataset/{id}/resource/{resource_id}/localise', controller=controller, action='localise')
        return map

    def after_map(self, map):
        return map

    def update_config(self, config):
        toolkit.add_template_directory(config, 'templates')


    def get_helpers(self):
        from ckanext.localiser.helpers import can_localise
        return {
            'can_localise': can_localise,
            'localiser_installed': lambda: True
        }