# -*- coding: utf-8 -*-
import unicodecsv as csv
import logging
import json
import os
import shutil
from urllib import unquote

from pylons import config
import ckan.logic as logic
import ckan.lib.navl.dictization_functions as dict_fns
import ckan.plugins.toolkit as toolkit

from ckan import model
from ckan.lib.base import c, h, BaseController, abort

from ckanext.localiser.lib.converters import get_converter

log = logging.getLogger(__name__)


class LocalController(toolkit.BaseController):
    """
    Processes requests related to localising CSV files
    """

    def localise(self, id, resource_id):
        ctx = {
            'model': model,
            'user': c.user
        }
        c.pkg_dict = logic.get_action("package_show")(ctx, {'id': id})
        c.resource = logic.get_action('resource_show')(ctx, {'id': resource_id})

        if c.resource['resource_type'] not in  ['file.upload', 'file']:
            h.flash_notice('Only uploaded files can be localised - %s' % c.resource['resource_type'])
            h.redirect_to(controller='package', action='resource_read', id=id, resource_id=resource_id)

        filepath = self.url_to_path(c.resource['url'])
        headers = self.header_list(filepath)

        if 'Latitude' in headers or 'Longitude' in headers:
            h.flash_notice('This resource already appears to contain location data')
            h.redirect_to(controller='package', action='resource_read', id=id, resource_id=resource_id)

        c.rownames = zip(range(len(headers)), headers)

        if toolkit.request.method == 'POST':
            data = toolkit.request.POST
            matches = {}
            for k, v in data.iteritems():
                if v:
                    matches[k] = v

            # invert matches and use the keys to determine which converter we want.
            args = {}
            for k, v in matches.iteritems():
                args[v] = int(k[len('field_'):])

            fn, getter = get_converter(args.keys())
            if not fn:
                # Bail with an error about not finding the converter.
                pass

            with open(filepath + '.new', 'w') as w:
                writer = csv.writer(w)
                with open(filepath, 'r') as f:
                    reader = csv.reader(f)
                    header_row = reader.next() # skip header
                    header_row.extend(['Latitude', 'Longitude'])
                    writer.writerow(header_row)

                    for row in reader:
                        lat,lng = fn(**getter(args, row))
                        row.extend([lat, lng])
                        writer.writerow(row)


            shutil.copy2(filepath + '.new', filepath)

            h.flash_notice('Lat/Lng data has been added to the resource')
            h.redirect_to(controller='package', action='resource_read', id=id, resource_id=resource_id)

        return toolkit.render('localiser/localise.html')

    def header_list(self, file):
        with open(file, 'r') as f:
            r = csv.reader(f)
            headers = r.next()
        return headers

    def url_to_path(self, url):
        # http://localhost:3008/storage/f/2014-08-11T19%3A14%3A51.219Z/stops.csv
        parts = url.split('/')
        path = '/'.join(parts[-2:])

        root =  os.path.join(config.get('ofs.storage_dir'), 'pairtree_root/de/fa/ul/t/obj')
        return os.path.join(root, unquote(path))