

def can_localise(resource):
    return (resource['resource_type'] in ['file.upload', 'file'] and
            resource['format'].lower() == 'csv')

