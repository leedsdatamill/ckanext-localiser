from ckanext.localiser.lib.en import bng_to_latlng

def en_to_latlng(easting, northing):
    return bng_to_latlng(float(easting), float(northing))

def postcode_to_latlng(postcode):
    return 1, 1

def get_en_vals(args, row):
    """
    Return a dict of values (to be passed in to the converter)
    """
    e = args['easting']
    n = args['northing']
    return {
        'easting': float(row[e]),
        'northing': float(row[n])
    }

def get_postcode_vals(args, row):
    pass

def get_converter(keys):
    """
    Returns a function for returning lat/lng for the data in the row,
    and a function that knows how to get all of the data out of the row
    ready for de-dicting when calling the converter.

    So, there's a function for converting easting and northing to lat/lng
    and a function that knows how to generate the arguments for it.
    """
    if 'easting' in keys and 'northing' in keys:
        return en_to_latlng, get_en_vals
    elif 'postcode' in keys:
        return postcode_to_latlng, get_postcode_vals
    return None, None